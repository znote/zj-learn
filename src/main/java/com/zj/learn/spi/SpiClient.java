package com.zj.learn.spi;

import com.zj.learn.spi.service.SpiInterface;

import java.util.ServiceLoader;

/**
 * test spi
 *
 * @author xi.yang(xi.yang@downjoy.com)
 * @create 2018-02-28 上午 10:17
 **/
public class SpiClient {
    public static void main(String[] args) {
        ServiceLoader<SpiInterface> loader = ServiceLoader.load(SpiInterface.class);
        for (SpiInterface spiInterface : loader) {
            if(3 == spiInterface.getVersion()){
                System.out.println("========3========");
                spiInterface.sayHello();
            } else if (1 == spiInterface.getVersion()) {
                System.out.println("========1========");
                spiInterface.sayHello();
            }
        }
    }
}
