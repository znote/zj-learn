package com.zj.learn.spi.service;

/**
 * spi接口
 *
 * @author xi.yang(xi.yang@downjoy.com)
 * @create 2018-02-28 上午 10:12
 **/
public interface SpiInterface {
    int getVersion();
    void sayHello();
}
