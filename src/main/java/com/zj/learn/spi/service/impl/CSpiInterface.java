package com.zj.learn.spi.service.impl;

/**
 * c
 *
 * @author xi.yang(xi.yang@downjoy.com)
 * @create 2018-02-28 上午 10:13
 **/
public class CSpiInterface extends BSpiInterface {
    @Override
    public int getVersion() {
        return 3;
    }

    @Override
    public void sayHello() {
        super.sayHello();
        System.out.println("this is c");
    }
}
