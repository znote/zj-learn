package com.zj.learn.spi.service.impl;

import com.zj.learn.spi.service.SpiInterface;

/**
 * a
 *
 * @author xi.yang(xi.yang@downjoy.com)
 * @create 2018-02-28 上午 10:13
 **/
public class ASpiInterface implements SpiInterface {
    @Override
    public int getVersion() {
        return 1;
    }

    @Override
    public void sayHello() {
        System.out.println("this is a");
    }
}
