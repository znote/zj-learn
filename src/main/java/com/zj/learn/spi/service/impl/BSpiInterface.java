package com.zj.learn.spi.service.impl;

/**
 * b
 *
 * @author xi.yang(xi.yang@downjoy.com)
 * @create 2018-02-28 上午 10:13
 **/
public class BSpiInterface extends ASpiInterface {
    @Override
    public int getVersion() {
        return 2;
    }

    @Override
    public void sayHello() {
        super.sayHello();
        System.out.println("this is b");
    }
}
