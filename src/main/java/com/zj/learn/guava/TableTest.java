package com.zj.learn.guava;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnel;

import java.nio.charset.Charset;

/**
 * @author xi.yang
 * @create 2020-04-10 12:33
 **/
public class TableTest {
    // 预计元素个数
    private static int size = 1024 * 1024;

    private static BloomFilter<String> bloom = BloomFilter.create((Funnel<String>) (from, into) -> {
        // 自定义过滤条件 此处不做任何过滤
        into.putString(from, Charset.forName("UTF-8"));


    }, size);

    public static void main(String[] args) {
        Table<String, String, String> table = HashBasedTable.create();
        table.put("ouyangxi", "nan", "yangyangyang");
        table.put("ouyangjie", "nv", "yangyang");
        boolean contans = table.contains("ouyangjie","nv");
        System.out.println("contans=" + contans);

        bloom.put("ouyangxi");
        Boolean b = bloom.mightContain("ouyangxi");
        System.out.printf("b=" + b);
    }
}
