package com.zj.learn.excel;

import com.alibaba.excel.annotation.ExcelProperty;

/**
 * @author xi.yang
 * @create 2020-07-06 16:01
 **/
public class Kkt {
    @ExcelProperty(value = "产品名称")
    private String name;
    @ExcelProperty(value = "兑换数量")
    private int num;
    @ExcelProperty(value = "产品单价")
    private double price;
    @ExcelProperty(value = "总金额")
    private double total;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
}
