package com.zj.learn.excel;

import com.alibaba.excel.EasyExcel;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author xi.yang
 * @create 2020-07-02 11:13
 **/
public class EasyExcelTest {

    public static void main(String[] args) {
        final String excelPath = "C:\\Users\\d.cn\\Desktop\\kkt202002.xlsx";
        final String excelDzPath = "C:\\Users\\d.cn\\Desktop\\kktdz202002.xlsx";
        List<Kkt> kktList = EasyExcel.read(excelPath, Kkt.class, null).doReadAllSync();
        List<Kkt> kktdzList = EasyExcel.read(excelDzPath, Kkt.class, null).doReadAllSync();
        Set<String> errorNames = new HashSet<>();
        for (Kkt kkt : kktdzList) {
            Kkt exist = getExist(kkt.getName(), kktList);
            if (null == exist || exist.getTotal() != kkt.getTotal()) {
                errorNames.add(kkt.getName());
            }
        }
//        for (Kkt kkt : kktList) {
//            Kkt exist = getExist(kkt.getName(), kktdzList);
//            if (null == exist || exist.getTotal() != kkt.getTotal()) {
//                errorNames.add(kkt.getName());
//            }
//        }
        for (String errorName : errorNames) {
            System.out.println(errorName);
        }
    }

    private static Kkt getExist(String name, List<Kkt> kktList) {
        for (Kkt kkt : kktList) {
            if (kkt.getName().equalsIgnoreCase(name)) {
                return kkt;
            }
        }
        return null;
    }
}
