package com.zj.learn.excel;

import com.alibaba.excel.EasyExcel;
import org.apache.commons.exec.util.MapUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author xi.yang
 * @create 2020-07-02 11:13
 **/
public class PayOrderTest {
    private static Map<Integer, Integer> map = new HashMap() {{
        put(6, 50);
        put(12, 100);
        put(30, 250);
        put(68, 500);
        put(98, 750);
        put(128, 1000);
        put(198, 1500);
        put(328, 2500);
        put(648, 5000);
    }};

    public static void main(String[] args) {
//        11780000.能整除140左
        int big = 11780000;
        int num = 140;
        for (int i = 1; i < 100; i++) {
            if (big%(num + i) == 0) {
                System.out.println(num + i);
            }
            if (big%(num - i) == 0) {
                System.out.println(num - i);
            }
        }
//        int[] ids = {12582913,15728641,10485761,10485762,4194305,8388609,7340033,3145730,9437186,12582914,4194307,14680065,14680068};
//        final String excelPath1 = "C:\\Users\\d.cn\\Desktop\\c101充值\\C101充值数据.xls";
//        final String excelPath2 = "C:\\Users\\d.cn\\Desktop\\c101充值\\第二次内测.xls";
//        Map<String, Long> map1 = getAmountMap(excelPath1);
//        System.out.println("map1 total count :" + map1.values().stream().mapToLong(Long::longValue).sum());
//        Map<String, Long> map2 = getAmountMap(excelPath2);
//        System.out.println("map2 total count :" + map2.values().stream().mapToLong(Long::longValue).sum());
//        map2.forEach((k, v) -> {
//            if (map1.containsKey(k)) {
//                map1.put(k, map1.get(k) + v);
//            } else {
//                map1.put(k, v);
//            }
//        });
////        map1.forEach((k, v) -> System.out.println(k + "\t" + v));
//
//        List<McPlayer> mcPlayers1 = EasyExcel.read("C:\\Users\\d.cn\\Desktop\\c101充值\\第一次内测uid.xls", McPlayer.class, null).doReadAllSync();
//        List<McPlayer> mcPlayers2 = EasyExcel.read("C:\\Users\\d.cn\\Desktop\\c101充值\\第一次内测uid.xls", McPlayer.class, null).doReadAllSync();
//        Map<String, String> faceBookId = new HashMap<>();
//        Map<String, String> googleId = new HashMap<>();
//        Map<String, String> uid = new HashMap<>();
//        for (McPlayer mcPlayer : mcPlayers1) {
//            final String mcId = mcPlayer.getMcId();
//            addMap(faceBookId, mcId, mcPlayer.getFaceBookId());
//            addMap(googleId, mcId, mcPlayer.getGoogelId());
//            addMap(uid, mcId, mcPlayer.getUid());
//        }
//        for (McPlayer mcPlayer : mcPlayers2) {
//            final String mcId = mcPlayer.getMcId();
//            addMap(faceBookId, mcId, mcPlayer.getFaceBookId());
//            addMap(googleId, mcId, mcPlayer.getGoogelId());
//            addMap(uid, mcId, mcPlayer.getUid());
//        }
//        map1.forEach((k, v) -> System.out.println(k + "\t" + v + "\t" + faceBookId.getOrDefault(k, "") + "\t" + googleId.getOrDefault(k, "") + "\t" + uid.getOrDefault(k, "")));
    }

    private static void addMap(Map<String, String> faceBookId, String mcId, String add) {
        if (add == null || "".equals(add)) {
            return;
        }
        List<String> old = new ArrayList<>(Arrays.asList(faceBookId.getOrDefault(mcId, "").replace("，", ",").split(",")));
        for (String s : add.replace("，", ",").split(",")) {
            if (old.contains(s)) {
                continue;
            }
            old.add(s);
        }
        faceBookId.put(mcId, String.join(",", old));
    }

    public static Map<String, Long> getAmountMap(String excelPath) {
        System.out.println("======================");
        List<PayOrder> orders = EasyExcel.read(excelPath, PayOrder.class, null).doReadAllSync();
        System.out.println(excelPath + "=====" + orders.size() + "====" + orders.stream().mapToLong(PayOrder::getAmount).sum());
        Map<Long, List<PayOrder>> gidMap = new HashMap<>();
        for (PayOrder order : orders) {
            if (order.getStatus() != 2) {
                continue;
            }
            List<PayOrder> list = gidMap.getOrDefault(order.getGid(), new ArrayList<>());
            list.add(order);
            // 2021/8/9 金额兑换成钻石
            int oldAmount = order.getAmount();
            Integer nAmount = map.get(oldAmount);
            if (nAmount == null) {
                System.out.println("====================================="+oldAmount);
            }
            order.setAmount(nAmount);
            gidMap.put(order.getGid(), list);
        }
        Map<String, Long> amounts = new HashMap<>();
        for (List<PayOrder> value : gidMap.values()) {
            String key = String.join(",", value.stream().map(PayOrder::getMcId).collect(Collectors.toSet()));
            long amount = value.stream().mapToLong(PayOrder::getAmount).sum();
            amounts.put(key, amounts.getOrDefault(key, 0L) + amount);
        }

//        amounts.forEach((k, v) -> System.out.println(k + "\t" + v));
        return amounts;
    }
}
