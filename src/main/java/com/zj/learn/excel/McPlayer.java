package com.zj.learn.excel;

import com.alibaba.excel.annotation.ExcelProperty;

/**
 * @author xi.yang
 * @create 2020-07-06 16:01
 **/
public class McPlayer {
    @ExcelProperty(value = "发行用户id")
    private String uid;
    @ExcelProperty(value = "Facebook三方id")
    private String faceBookId;
    @ExcelProperty(value = "Google三方id")
    private String googelId;
    @ExcelProperty(value = "机器码")
    private String mcId;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getFaceBookId() {
        return faceBookId;
    }

    public void setFaceBookId(String faceBookId) {
        this.faceBookId = faceBookId;
    }

    public String getGoogelId() {
        return googelId;
    }

    public void setGoogelId(String googelId) {
        this.googelId = googelId;
    }

    public String getMcId() {
        return mcId;
    }

    public void setMcId(String mcId) {
        this.mcId = mcId;
    }
}
