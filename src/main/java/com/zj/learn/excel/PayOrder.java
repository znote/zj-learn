package com.zj.learn.excel;

import com.alibaba.excel.annotation.ExcelProperty;

/**
 * @author xi.yang
 * @create 2020-07-06 16:01
 **/
public class PayOrder {
    @ExcelProperty(value = "gid")
    private long gid;
    @ExcelProperty(value = "pid")
    private String pid;
    @ExcelProperty(value = "机器码")
    private String mcId;
    @ExcelProperty(value = "amount")
    private int amount;
    @ExcelProperty(value = "status")
    private int status;

    @Override
    public String toString() {
        return "PayOrder{" +
                "gid=" + gid +
                ", pid='" + pid + '\'' +
                ", mcId='" + mcId + '\'' +
                ", amount=" + amount +
                ", status=" + status +
                '}';
    }

    public long getGid() {
        return gid;
    }

    public void setGid(long gid) {
        this.gid = gid;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getMcId() {
        return mcId;
    }

    public void setMcId(String mcId) {
        this.mcId = mcId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
