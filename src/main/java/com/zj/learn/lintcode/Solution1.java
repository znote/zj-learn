package com.zj.learn.lintcode;

public class Solution1 {
    /**
     * @param a: An integer
     * @param b: An integer
     * @return: The sum of a and b 
     */
    public int aplusb(int a, int b) {
    	if (b == 0)
    		return a;
    	int temp1 = a^b;
    	int temp2 = (a&b) << 1;
    	return aplusb(temp1, temp2);
    }
}