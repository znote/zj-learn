package com.zj.learn.lintcode;

public class Solution13 {
    /**
     * @param source: 
     * @param target: 
     * @return: return the index
     */
    public int strStr(String source, String target) {
        // Write your code here
        return source.indexOf(target);
    }

    public static void main(String[] args) {
        System.out.println(new Solution13().strStr("abcddsfousdfsdf","dds"));
    }
}