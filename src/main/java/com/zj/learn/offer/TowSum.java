package com.zj.learn.offer;

public class TowSum {
    /**
     * @param numbers: An array of Integer
     * @param target: target = numbers[index1] + numbers[index2]
     * @return: [index1, index2] (index1 < index2)
     */
    public int[] twoSum(int[] numbers, int target) {
        // write your code here
        for (int i = 0; i < numbers.length; i++) {
            for (int j = numbers.length-1; j > 0; j--) {
                if (i >= j) {
                    continue;
                }
                if (numbers[i] + numbers[j] == target) {
                    int[] re = {i,j};
                    return re;
                }
            }
        }
        return null;
    }

    public static void main(String[] args) {
        int[] numbers = {2,7,11,15};
        int target = 9;
        System.out.println(new TowSum().twoSum(numbers, target));
    }
}