package com.zj.learn.offer.clazz;

/**
 * @author xi.yang
 * @create 2021-03-04 15:26
 **/
public class Third extends Second{
    static{
        System.out.println("Third static");
    }

    public Third() {
        System.out.println("Third Constructor");
    }

    public static void main(String[] args) {
        new Third();
    }
}
