package com.zj.learn.offer.clazz;

/**
 * @author xi.yang
 * @create 2021-03-04 15:26
 **/
public class First {
    static{
        System.out.println("First static");
    }

    public First() {
        System.out.println("First Constructor");
    }
}
