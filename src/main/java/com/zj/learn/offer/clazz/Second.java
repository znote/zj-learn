package com.zj.learn.offer.clazz;

/**
 * @author xi.yang
 * @create 2021-03-04 15:26
 **/
public class Second extends First{
    static{
        System.out.println("Second static");
    }

    public Second() {
        System.out.println("Second Constructor");
    }
}
