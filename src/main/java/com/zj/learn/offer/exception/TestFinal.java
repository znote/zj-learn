package com.zj.learn.offer.exception;

/**
 * @author xi.yang
 * @create 2021-03-01 17:57
 **/
public class TestFinal {
    public static void main(String[] args) {
        System.out.println(fun());
    }

    private static int fun() {
        int a = 3;
        int b = 5;
        int c = 10;
        int result = 0;
        try {
            result = a + b;
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("do finally");
            result += c;
        }
        return result;
    }
}
