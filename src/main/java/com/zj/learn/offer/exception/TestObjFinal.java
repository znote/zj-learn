package com.zj.learn.offer.exception;

/**
 * @author xi.yang
 * @create 2021-03-01 17:57
 **/
public class TestObjFinal {
    public static void main(String[] args) {
        System.out.println(fun());
    }

    private static StringBuilder fun() {
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("3");
            return sb;
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            sb.append("5");
        }
        return sb;
    }
}
