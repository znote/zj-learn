package com.zj.learn.offer.obj;

/**
 * @author xi.yang
 * @create 2021-03-01 17:51
 **/
public class Cat extends Animal{
    public static void say() {
        System.out.println("cat say");
    }

    public void eat() {
        System.out.println("cat eat");
    }
}
