package com.zj.learn.offer.obj;

/**
 * @author xi.yang
 * @create 2021-03-01 17:52
 **/
public class Test {
    public static void main(String[] args) {
        Animal an = new Cat();
        an.say();
        an.eat();

        Cat cat = new Cat();
        cat.say();
        cat.eat();
    }
}
