package com.zj.learn.thread;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xi.yang
 * @create 2021-07-20 9:39
 **/
public class MyData {
    private static final List<Integer> list = new ArrayList<>();

    public static void add(Integer integer) {
        list.add(integer);
    }

    public static List<Integer> list() {
        return list;
    }
}
