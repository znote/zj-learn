package com.zj.learn.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author xi.yang
 * @create 2019-03-15 17:20
 **/
public class ThreadQuqueRun {
    static ExecutorService executorService = Executors.newSingleThreadExecutor();

    public static void main(String[] args) throws Exception {
//        test2();
        test1();
    }

    private static void test2() {
        Thread t1 = new Thread(() -> System.out.println("test2 t1 run ..."));
        Thread t2 = new Thread(() -> System.out.println("test2 t2 run ..."));
        Thread t3 = new Thread(() -> System.out.println("test2 t3 run ..."));
        System.out.println("test2 start");
        executorService.submit(t1);
        executorService.submit(t2);
        executorService.submit(t3);
        System.out.println("test2 end");
        executorService.shutdown();
    }

    private static void test1() throws InterruptedException {
        Thread t1 = new Thread(() -> System.out.println(Thread.currentThread().getName()+":test1 t1 run ..."),"t1");
        Thread t2 = new Thread(() -> System.out.println(Thread.currentThread().getName()+":test1 t2 run ..."),"t2");
        Thread t3 = new Thread(() -> System.out.println(Thread.currentThread().getName()+":test1 t3 run ..."),"t3");
        System.out.println(Thread.currentThread().getName() + ":test1 start");
        t1.start();
        t1.join();
        t2.start();
        t2.join();
        t3.start();
        t3.join();
        System.out.println(Thread.currentThread().getName() + ":test1 end");
    }


}
