package com.zj.learn.thread;

/**
 * @author xi.yang
 * @create 2021-07-20 9:39
 **/
public class ThreadWait implements Runnable{
    Object object;

    public ThreadWait(Object object) {
        this.object = object;
    }

    @Override
    public void run() {
        synchronized (object) {
            for (int i = 0; i < 15; i++) {
                System.out.println("list size = "+MyData.list().size());
                if (MyData.list().size() == 10) {
                    try {
                        System.out.println("=========wait start======");
                        object.wait();
                        System.out.println("=========wait end======");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                MyData.add(i);
                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
