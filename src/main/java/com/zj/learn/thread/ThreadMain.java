package com.zj.learn.thread;

/**
 * @author xi.yang
 * @create 2021-07-20 9:50
 **/
public class ThreadMain {
    public static void main(String[] args) {
        Object o = new Object();
        new Thread(new ThreadWait(o)).start();
        try {
            Thread.sleep(11000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new Thread(new ThreadNotify(o)).start();
    }
}
