package com.zj.learn.thread;

/**
 * @author xi.yang
 * @create 2021-07-20 9:44
 **/
public class ThreadNotify implements Runnable {
    Object object;

    public ThreadNotify(Object object) {
        this.object = object;
    }

    @Override
    public void run() {
        synchronized (object){
            System.out.println("list size = "+MyData.list().size());
            if (MyData.list().size() > 5) {
                System.out.println("=================notify start===============");
                object.notify();
                try {
                    Thread.sleep(5000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("=================notify end===============");
            }
        }
    }
}
