package com.zj.learn.datetime;

import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * @author xi.yang
 * @create 2020-11-24 11:34
 **/
public class DateTimeApi {
    public static void main(String[] args) {
        ZonedDateTime dateTime = ZonedDateTime.now(ZoneId.of("UTC+8"));
        ZonedDateTime dateTime1 = ZonedDateTime.now(ZoneId.of("UTC-3"));
        System.out.println(dateTime);
        System.out.println(dateTime1);
        ZonedDateTime dateTime2 = ZonedDateTime.now(ZoneId.of("BET",ZoneId.SHORT_IDS));
        System.out.println(dateTime2);
//        LocalDateTime localDateTime = LocalDateTime.now();
//        System.out.println(localDateTime.format(DateTimeFormatter.BASIC_ISO_DATE));
//        System.out.println(localDateTime.format(DateTimeFormatter.ISO_DATE));
//        System.out.println(localDateTime.format(DateTimeFormatter.ISO_TIME));
//        System.out.println(localDateTime.format(DateTimeFormatter.ISO_DATE_TIME));
////        System.out.println(localDateTime.format(DateTimeFormatter.ISO_INSTANT));
//        System.out.println(localDateTime.format(DateTimeFormatter.ISO_LOCAL_DATE));
//        System.out.println(localDateTime.format(DateTimeFormatter.ISO_LOCAL_TIME));
//        System.out.println(localDateTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
//        System.out.println(localDateTime.format(DateTimeFormatter.ISO_ORDINAL_DATE));
//        System.out.println(localDateTime.format(DateTimeFormatter.ISO_WEEK_DATE));
    }
}
