package com.zj.learn.flux;

import java.util.function.IntUnaryOperator;

/**
 * @author xi.yang
 * @create 2019-02-12 16:05
 **/
public class MethodRefrenceDemo {
    public static void main(String[] args) {
        IntUnaryOperator intUnaryOperator1 = DemoClass::staticMethod;
        DemoClass demoClass = new DemoClass();
        IntUnaryOperator intUnaryOperator2 = demoClass::normalMethod;
        System.out.println(intUnaryOperator1.applyAsInt(11));
        System.out.println(intUnaryOperator2.applyAsInt(11));

        Demo2Class demo2Class = new Demo2Class();
        demo2Class.normalMethod(12);
    }
}

class DemoClass {

    /**
     * 这里是一个静态方法
     */
    public static int staticMethod(int i) {
        System.out.println("静态方法不可以访问this:");
        return i * 2;
    }

    /**
     * 这里是一个实例方法
     */
    public int normalMethod(int i) {
        System.out.println("实例方法可以访问this:" + this);
        return i * 3;
    }

}

class Demo2Class {
    public int normalMethod(Demo2Class this, int i) {
        System.out.println("实例方法可以访问this:" + this);
        return i * 3;
    }
}