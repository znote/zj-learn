package com.zj.learn.flux;

import java.util.stream.IntStream;

/**
 * @author xi.yang
 * @create 2019-02-13 9:29
 **/
public class StreamDemo {
    public static void main(String[] args) {
        int[] nums = {1, 2, 3, 4, 5, 6, 7, 8};
        int total = 0;
        for (int num : nums) {
            total += num;
        }
        System.out.println(total);

        int total2 = IntStream.of(nums).map(StreamDemo::doubleNumber).sum();
        System.out.println(total2);

        System.out.println("惰性求值测试");
        IntStream.of(nums).map(StreamDemo::doubleNumber);
    }

    public static int doubleNumber(int i) {
        System.out.println("i*2后返回");
        return i * 2;
    }
}
