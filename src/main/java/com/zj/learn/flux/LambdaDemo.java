package com.zj.learn.flux;

/**
 * lambda表达式测试
 *
 * @author xi.yang
 * @create 2019-02-12 14:57
 **/
public class LambdaDemo {
    private String name = "LambdaDemo";
    public void test() {
        new Thread(new Runnable() {
            private String name = "Runnalbe";
            @Override
            public void run() {
                System.out.println("this value1 = ："+this.name);
            }
        }).start();

        new Thread(() -> System.out.println("this value2 = "+this.name)).start();
    }

    public static void main(String[] args) {
        new LambdaDemo().test();
    }
}
