package com.zj.learn.constant;

import com.alibaba.fastjson.JSON;

import java.io.*;

public class TestConstant {
    public static void main(String[] args) {
        final String path = "stud.obj";
//        Stud stud = new Stud();
//        stud.setId(100);
//        writeObject(path, stud);

        Stud stud = (Stud) readObject(path);
        System.out.printf("==="+ JSON.toJSONString(stud));
    }
    /**
     * 保存序列化对象
     * @param path
     * @param object
     */
    public static void writeObject(String path,Object object) {
        try {
            ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(new File(path)));
            outputStream.writeObject(object);
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 读取序列化对象
     * @param path
     * @return
     */
    public static Object readObject(String path) {
        try {
            ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(new File(path)));
            Object object = inputStream.readObject();
            inputStream.close();
            return object;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
