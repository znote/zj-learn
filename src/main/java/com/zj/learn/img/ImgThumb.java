package com.zj.learn.img;

import net.coobird.thumbnailator.Thumbnails;

import java.io.File;
import java.io.IOException;

/**
 * @author xi.yang
 * @create 2021-03-05 5:30 PM
 **/
public class ImgThumb {
    static String src = "C:\\Users\\d.cn\\Desktop\\dd\\1";
    static String dist = "C:\\Users\\d.cn\\Desktop\\dd\\2";
    public static void main(String[] args) throws IOException {

        File file = new File(src);
        reduceImgAll(file);
    }

    private static void reduceImgAll(File file) throws IOException {
        if (file.isDirectory()) {
            for (File listFile : file.listFiles()) {
                reduceImgAll(listFile);
            }
        } else {
            String distPath = file.getPath().replace(src, dist);
            mkdirs(new File(distPath).getParent());
            Thumbnails.of(file).scale(0.5).toFile(distPath);
        }
    }

    public static void mkdirs(String dirsPath) {
        File file = new File(dirsPath);
        if (!file.exists()) {
            file.mkdirs();
        }
    }
}
