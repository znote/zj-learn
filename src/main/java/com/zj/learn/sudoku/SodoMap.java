package com.zj.learn.sudoku;

/**
 * @author xi.yang
 * @create 2021-08-28 15:25
 **/
public class SodoMap {
    public static final String sdq5_1 =
            "x4x xxx xxx" +
                    "xxx 56x xx9" +
                    "xx7 8xx 1xx" +
                    "2x3 xxx x5x" +
                    "xx5 x8x x31" +
                    "6xx xxx 8xx" +
                    "3x6 9xx 7xx" +
                    "x54 x3x xxx" +
                    "xxx x51 xxx";

    public static final String sdq10_8 =
            "xxx xx1 xx7" +
                    "xxx x6x x2x" +
                    "8xx 9xx 3xx" +
                    "x95 4xx xx3" +
                    "xx3 xxx 4xx" +
                    "4xx xxx x8x" +
                    "xxx xx7 xx6" +
                    "x1x x2x xxx" +
                    "5xx 3xx 9xx";

    public static final String sdq1_15 = "x546xx9xxxx89xxxxxxxx3xxx7xx9xxxxxx4xxxxx3xxxx8xx5x1xx2xxxxxx6xxxx82xxx7x694xxx23";

}
