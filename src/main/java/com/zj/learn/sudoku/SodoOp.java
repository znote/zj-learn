package com.zj.learn.sudoku;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author xi.yang
 * @create 2021-07-31 17:23
 **/
public class SodoOp {
    public static void sodoValRm(Sodo sodo, int rmNum) {
        if (sodo.isOk()) {
            return;
        }
        boolean opResult = sodo.getVal().remove(rmNum);
        if (opResult) {
            if (sodo.isOk()) {
                sodo.colOpSodo();
            } else {
                sodo.changeVal();
            }
        }
    }

    public static boolean containsSodo(Set<Sodo> v, Sodo sodo) {
        return v.stream().mapToInt(Sodo::getIndex).boxed().collect(Collectors.toList()).contains(sodo.getIndex());
    }
}
